<?php
/**
 * @file
 *
 */

/**
 * Form of the mapping, map CCK fields to ax schema attributes
 */
function openid_profile_map($form, $form_state) {
  $mapping = array();
  $source = openid_profile_get_mapping($mapping);
  $already_mapped = array_keys($mapping);
  $options_user = array();
  $user_fields = array(); // used for mapping_name => label value

  // Define general profile fields
  $options_profile = array(
    'name' => t('Account: Username'),
    'mail' => t('Account: Email'),
    'picture' => t('Account: Picture'),
    'timezone' => t('Account: Timezone'),
    'language' => t('Account: Language'),
  );
  foreach ($options_profile as $field_name => $field_label) {
    $user_fields[$field_name] = $field_label;
    if (!in_array($field_name, $mapping)) {
      $options_user += array($field_name => $field_label);
    }
  }

  // Define additional user profile fileds
  $instances = field_read_instances(array('entity_type' => 'user', 'bundle' => 'user'));
  foreach ($instances as $field_instance) {
    $field_name = $field_instance['field_name'];
    $field_info = field_info_field($field_name);

    foreach ($field_info['columns'] as $key => $coldata) {    
      // Skip the format column on text fields
      if ($key == 'format' && $field_info['type'] == 'text')
        continue;
      
      $field_label = $field_instance['label'];
      if (!empty($coldata['description'])) {
        $field_label .= ": " . $coldata['description'];
      }
      $mapped_field_key = $field_name . ":" . $key;
      $user_fields[$mapped_field_key] = $field_label;
      if (!in_array($mapped_field_key, $mapping)) {
        $options_user += array($mapped_field_key => $field_label);
      }
    }
  }

  //TODO: support deprecated core profile module
  // Add core profile fields
  /*
  if (module_exists('profile')) {
    foreach (openid_profile_get_profile_fields() as $profile_field => $values) {
      $options_profile += array($profile_field => t('Profile') . ': ' . $values['title'] . ' (' . $values['category'] . ')');
    }
  }
  */

  // Allow other modules to hook into the profile field definition
  drupal_alter('openid_profile', $options_node);

  $options_openid = array();
  $openid_attrs = openid_ax_api_schema_definitions();

  if (module_exists('openid_provider_ax') || module_exists('openid_client_ax')) {
    foreach ($openid_attrs as $id => $attr) {
      if (!in_array($id, $already_mapped)) {
        $options_openid += array($id => 'AX: ' . $attr['label'] . ' (' . $attr['description'] . ')');
      }
    }
  }

  $openid_sreg_fields = array();
  if (module_exists('openid_provider_sreg') || module_exists('openid_client_sreg')) {
    foreach ($openid_attrs as $id => $attr) {
      if (isset($attr['sreg']) && !in_array($attr['sreg'], $already_mapped)) {
        $openid_sreg_fields += array($attr['sreg'] => 'SReg: ' . $attr['label']);
      }
    }
  }

  if (!module_exists('openid_client_ax') && !module_exists('openid_provider')) {
    $form['warning']['#value'] = t('Enable OpenID Client AX module to use this module on an OpenID client site.');
    return $form;
  }
  elseif (!(module_exists('openid_provider_sreg') || module_exists('openid_provider_ax')) && !module_exists('openid_client_ax')) {
    $form['warning']['#value'] = t('Enable OpenID Provider SReg, OpenID Provider AX or both to use the module on an OpenID provider site.');
    return $form;
  }

  // Collect sreg labels
  $sreg_fields = array();
  foreach ($openid_attrs as $id => $attr) {
    if (isset($attr['sreg'])) {
      $sreg_fields[$attr['sreg']] = $attr['label'];
    }
  }

  // Generate table for showing the current mapping
  $rows = array();
  foreach ($mapping as $openid => $field_name) {
    if (!strstr($openid, '.sreg.')) {
      $rows[] = array(
        'node' => $user_fields[$field_name],
        'openid' => 'AX: ' . $openid_attrs[$openid]['label'] . ' (' . $openid_attrs[$openid]['description'] . ')',
        'link' => l(t('Delete'), "openid_profile/delete/" . base64_encode($openid)),
      );
    }
    else {
      $rows[] = array(
        'node' => $user_fields[$field_name],
        'openid' => 'SReg: ' . $sreg_fields[$openid],
        'link' => l(t('Delete'), "openid_profile/delete/" . base64_encode($openid) . '/sreg'),
      );
    }
  }

  $current_map = theme('table', array('header' => array('node' => t('Field'), 'openid' => t('OpenID field'), 'link' => t('Operation')),
                                      'rows' => $rows,
                                      'empty' => t('No mapping defined.')));

  if ($source == OPENID_PROFILE_DEFAULT) {
    $status = t('(default)');
  }
  elseif ($source == OPENID_PROFILE_OVERRIDDEN) {
    $status = t('(default, <em>overridden</em>)');
  }
  else { // OPENID_PROFILE_NORMAL
    $status = '';
  }
  $form['current_map'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mappings !status', array('!status' => $status)),
    '#collapsible' => FALSE,
  );
  $form['current_map']['map'] = array(
    '#markup' => isset($current_map) ? $current_map : '',
  );
  if ($source == OPENID_PROFILE_OVERRIDDEN) {
    $form['current_map']['revert'] = array(
      '#type' => 'submit',
      '#value' => t('Revert'),
      '#submit' => array('openid_profile_map_revert_submit'),
    );
  }
  $form['map'] = array(
    '#type' => 'fieldset',
    '#title' => t('Map user fields to OpenID fields'),
  );
  if (count($options_user) == 0 || (count($openid_attrs) == 0 && count($openid_sreg_fields) == 0)) {
    $form['map']['info'] = array('#value' => t('No more fields to map.'));
  }
  else {
    $form['#attached']['css'] = array(drupal_get_path('module', 'openid_profile') . "/openid_profile.css");
    $form['map']['map_a'] = array(
      '#type' => 'select',
      '#title' => t('Node field'),
      '#options' => $options_user,
    );
    $form['map']['map_b'] = array(
      '#type' => 'select',
      '#title' => t('OpenID field'),
      '#options' => $options_openid + $openid_sreg_fields,
    );
    $form['map']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
      '#submit' => array('openid_profile_map_add_submit'),
    );
    $form['map']['#description'] = t('Fields defined by the <a href="http://openid.net/specs/openid-attribute-exchange-1_0.html">Attribute Exchange</a> specification are prefixed AX, fields defined by the <a href="http://openid.net/specs/openid-simple-registration-extension-1_0.html">Simple Registration</a> specification are prefixed SReg.');
  }

  return $form;
}

/**
 * Saves the user-defined mapping.
 */
function openid_profile_map_add_submit($form, &$form_state) {
  $mapping = array();
  openid_profile_get_mapping($mapping);
  if (!empty($form_state['values']['map_b']) && !empty($form_state['values']['map_a'])) {
    $mapping += array(
      $form_state['values']['map_b'] => $form_state['values']['map_a'],
    );
    variable_set('openid_profile_map', $mapping);
  }
}

function openid_profile_map_revert_submit($form, &$form_state) {
  $mapping_imported = module_invoke_all('openid_profile_mapping');
  variable_set('openid_profile_map', $mapping_imported[$type]);
}

/**
 * Shows the current mapping code to the user.
 */
function openid_profile_export($form, $form_state, $type) {
  $mapping = array();
  openid_profile_get_mapping($mapping, $type);
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $array = '$mapping = array();';
  $head = "\$mapping[\"$type\"] = ";
  $tail = ';';
  $return = '
return $mapping;';
  $form['show'] = array(
    '#title' => t('PHP code'),
    '#type' => 'textarea',
    '#default_value' => $array . $head . var_export($mapping, TRUE) . $tail . $return,
    '#description' => t('This code can be pasted into an implementation of hook_openid_profile_mapping().'),
  );
  return $form;
}

/**
 * Deletes the user-defined mapping
 */
function openid_profile_delete() {
  $openid = base64_decode(arg(2));
  $mapping = array();
  $source = openid_profile_get_mapping($mapping);
  if ($source == OPENID_PROFILE_DEFAULT) {
    variable_set('openid_profile_map', $mapping);
  }
  $mapping = variable_get('openid_profile_map', array());
  unset($mapping[$openid]);
  variable_set('openid_profile_map', $mapping);
  drupal_goto('admin/config/people/accounts/openid_ax_mapping/edit');
}
